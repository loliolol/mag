package com.company.goods;

import com.company.interfaces.IDepartment;
import com.company.interfaces.IElectronicDevice;

public class ElectronicDevice extends BaseGoods implements IElectronicDevice {


    public ElectronicDevice(IDepartment department, String name, boolean hasGuarantee, String price, String company) {
        super(department, name, hasGuarantee, price, company);
    }

    public ElectronicDevice(String name) {
        super(name);
    }

    public ElectronicDevice() {
        super();
    }
}
