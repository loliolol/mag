package com.company.start;

import com.company.SalesRoom;
import com.company.clients.Visitor;
import com.company.departments.ElectronicDepartment;
import com.company.departments.GameDepartment;
import com.company.departments.HomeDepartment;
import com.company.enums.ConsultResult;
import com.company.goods.Computer;
import com.company.goods.GameConsole;
import com.company.goods.HardDrive;
import com.company.goods.Televisor;
import com.company.service.*;

public class Main {
    static final String SHOP_NAME = "Эльдорадо";
    public static void imitateShopWorking(){

        SalesRoom saleslRoom1 = new SalesRoom(SHOP_NAME);
        Administrator administrator = new Administrator();
        administrator.addSalesRoom(saleslRoom1);

        ElectronicDepartment electronicDepartment = new ElectronicDepartment();
        GameDepartment gameDepartment = new GameDepartment();

        saleslRoom1.addDepartment(electronicDepartment);
        saleslRoom1.addDepartment(gameDepartment);


        Banker banker_game = new Banker(true);
        Cashier cashier_game = new Cashier(true);
        Consultant consultant_game = new Consultant(true);
        Security security_game = new Security(true);

        Banker banker_electron = new Banker(true);
        Cashier cashier_electron = new Cashier(true);
        Consultant consultant_electron = new Consultant(true);
        Security security_electron = new Security(true);

        electronicDepartment.addEmployee(banker_electron);
        electronicDepartment.addEmployee(cashier_electron);
        electronicDepartment.addEmployee(consultant_electron);
        electronicDepartment.addEmployee(security_electron);

        gameDepartment.addEmployee(banker_game);
        gameDepartment.addEmployee(cashier_game);
        gameDepartment.addEmployee(consultant_game);
        gameDepartment.addEmployee(security_game);

        Computer computer_game = new Computer("Крутой компьютер");
        HardDrive hardDrive_game = new HardDrive("Крутой жесктий диск");
        Televisor televisor_game = new Televisor("Крутой жесктий диск");
        GameConsole gameConsole_game = new GameConsole();

        Computer computer_electron = new Computer("Крутой компьютер");
        HardDrive hardDrive_electron = new HardDrive("Крутой жесктий диск");
        Televisor televisor_electron = new Televisor("Крутой жесктий диск");
        GameConsole gameConsole_electron = new GameConsole();

        gameDepartment.addGood(computer_game);
        gameDepartment.addGood(hardDrive_game);
        gameDepartment.addGood(televisor_game);
        gameDepartment.addGood(gameConsole_game);

        electronicDepartment.addGood(computer_electron);
        electronicDepartment.addGood(hardDrive_electron);
        electronicDepartment.addGood(televisor_electron);
        electronicDepartment.addGood(gameConsole_electron);

        Visitor v1 = new Visitor("Виталик");
        Visitor v2 = new Visitor("Дед мороз");

        Consultant consultant_v1 = administrator.getFreeConsultant(electronicDepartment);
        Consultant consultant_v2 = administrator.getFreeConsultant(gameDepartment);

        ConsultResult consultResult = consultant_v1.consult(v1);
        ConsultResult consultResult_2 = consultant_v2.consult(v2);

        switch (consultResult) {
            case BUY:
                v1.buy(televisor_game);
                break;
            case EXIT:
                break;
        }

        switch (consultResult_2) {
            case BUY:
                v2.buy(computer_game);
                break;
            case EXIT:
                break;
        }
    }

    public static void main(String[] args) {
        imitateShopWorking();
    }
}
