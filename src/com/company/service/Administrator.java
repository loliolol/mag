package com.company.service;

import com.company.SalesRoom;
import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IEmployee;

import java.util.ArrayList;

public class Administrator {
    private String name;
    ArrayList <SalesRoom> salesRoom = new ArrayList<>();
    private boolean free;

    public Administrator(String name, ArrayList<SalesRoom> salesRoom, boolean free) {
        this.name = name;
        this.salesRoom = salesRoom;
        this.free = free;
    }

    public Administrator() {

    }

    public void addSalesRoom(SalesRoom salesroom){
        salesRoom.add(salesroom);
    }

    public Consultant getFreeConsultant(IDepartment iDepartment) {
        for (IEmployee employee : iDepartment.getEmployEesList()) {
            if (employee instanceof Consultant) {
                if (employee.isFree()) {
                    return (Consultant)employee;
                }
            }
        }
        return null;
    }


    public boolean isFree() {
        return free;
    }
}
