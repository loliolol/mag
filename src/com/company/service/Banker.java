package com.company.service;

import com.company.bank.BaseBank;
import com.company.departments.BaseDepartment;
import com.company.interfaces.IBank;
import com.company.interfaces.IDepartment;

public class Banker extends BaseEmployee{
    private IBank BaseBank;

    public Banker(String name, boolean free, IDepartment department, IBank baseBank) {
        super(name, free, department);
        BaseBank = baseBank;
    }

    public Banker(boolean free) {
        super(free);
    }

    public void sendRequest(){

    }

}
